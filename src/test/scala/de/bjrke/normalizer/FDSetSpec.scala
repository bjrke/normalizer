package de.bjrke.normalizer

import org.scalatest.Matchers
import org.scalatest.refspec.RefSpec

import Normalizer._

class FDSetSpec extends RefSpec with Matchers {

  object `method closureOfAttributes` {

    def `should return parameters for empty map`() {
      FDSet().closureOfAttributes(Set("a")) shouldBe Set("a")
    }

    def `should return empty for empty parameter`() {
      FDSet(Set("a") -> Set("b")).closureOfAttributes(Set()) shouldBe Set()
    }

    def `should return parameter if not matched`() {
      FDSet(Set("a") -> Set("b")).closureOfAttributes(Set("x")) shouldBe Set(
        "x")
    }

    def `should return ab for a->b and input a`() {
      FDSet(Set("a") -> Set("b"))
        .closureOfAttributes(Set("a")) shouldBe Set("a", "b")
    }

    def `should return abc for a->b->c`() {
      FDSet(Set("a") -> Set("b"), Set("b") -> Set("c"))
        .closureOfAttributes(Set("a")) shouldBe Set("a", "b", "c")
    }

    def `should return abc for a->b ab->c`() {
      FDSet(Set("a") -> Set("a", "b"), Set("b") -> Set("c"))
        .closureOfAttributes(Set("a")) shouldBe Set("a", "b", "c")
    }

  }

  object `method rightReduction` {
    def `should remove transitive dependency`() {
      FDSet()
        .withDep("a", Set("b", "c"))
        .withDep("b", "c")
        .rightReduction shouldBe FDSet(Set("a") -> Set("b"),
                                       Set("b") -> Set("c"))

    }
  }

  object `method leftReduction` {
    def `should remove redundant dependecy`()() {
      FDSet()
        .withDep(Set("a", "b"), Set("c"))
        .withDep("b", "c")
        .leftReduction shouldBe FDSet(Set("b") -> Set("c"))
    }
  }

}
