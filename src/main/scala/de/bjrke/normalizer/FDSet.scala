package de.bjrke.normalizer
import scala.annotation.tailrec
import scala.collection.immutable.{AbstractMap, Map, Set}

case class FDSet(private val delegate: Map[Set[String], Set[String]])
    extends AbstractMap[Set[String], Set[String]] {

  override def +[V1 >: Set[String]](fd: (Set[String], V1)): FDSet = fd match {
    case (key, value: Set[String]) => withDep(key, value)
  }

  def withDep(determinant: Set[String], dependent: Set[String]): FDSet =
    this(determinant).union(dependent).diff(determinant) match {
      case e if e.isEmpty => this
      case newDependent   => new FDSet(delegate + (determinant -> newDependent))
    }

  def withDep(determinant: String, dependent: Set[String]): FDSet =
    withDep(Set(determinant), dependent)

  def withDep(determinant: Set[String], dependent: String): FDSet =
    withDep(determinant, Set(dependent))

  def withDep(determinant: String, dependent: String): FDSet =
    withDep(Set(determinant), Set(dependent))

  override def get(determinant: Set[String]): Option[Set[String]] =
    delegate.get(determinant) match {
      case None          => FDSet.someEmptySet
      case someDepending => someDepending
    }

  override def iterator: Iterator[(Set[String], Set[String])] =
    delegate.iterator

  def deUnify: Iterator[(Set[String], String)] =
    toList.reverseIterator.flatMap {
      case (determinant, dependents) =>
        dependents.map(dependent => determinant -> dependent)
    }

  override def -(key: Set[String]): Map[Set[String], Set[String]] =
    FDSet(delegate - key)

  def withoutDep(determinant: Set[String], dependent: Set[String]): FDSet =
    this(determinant).diff(dependent) match {
      case e if e.isEmpty => FDSet(delegate - determinant)
      case newDependent   => FDSet(delegate + (determinant -> newDependent))
    }

  def withoutDep(determinant: String, dependent: Set[String]): FDSet =
    withoutDep(Set(determinant), dependent)

  def withoutDep(determinant: Set[String], dependent: String): FDSet =
    withoutDep(determinant, Set(dependent))

  def withoutDep(determinant: String, dependent: String): FDSet =
    withoutDep(Set(determinant), Set(dependent))

  @tailrec
  final def closureOfAttributes(result: Set[String]): Set[String] = {
    val splitRules = groupBy(_._1.forall(result))

    splitRules.get(true) match {
      case Some(rulesToApply) =>
        val nextResult = rulesToApply.foldLeft(result)((r, fd) => r ++ fd._2)
        splitRules.get(false) match {
          case Some(remainingRules) =>
            if (result != nextResult)
              FDSet(remainingRules).closureOfAttributes(nextResult)
            else nextResult
          case None => nextResult
        }
      case None => result
    }

  }

  @tailrec
  final def leftReduction: FDSet =
    deUnify
      .flatMap {
        case (alpha, beta) => alpha.map(a => (a, alpha, beta))
      }
      .find {
        case (a, alpha, beta) =>
          closureOfAttributes(alpha - a).contains(beta)
      } match {
      case Some((a, alpha, beta)) =>
        withoutDep(alpha, beta).withDep(alpha - a, beta).leftReduction
      case None => this
    }

  @tailrec
  final def rightReduction: FDSet =
    deUnify.find {
      case (alpha, beta) =>
        withoutDep(alpha, beta).closureOfAttributes(alpha).contains(beta)
    } match {
      case Some((alpha, beta)) => withoutDep(alpha, beta).rightReduction
      case None                => this
    }

  def canonicalCover: FDSet = leftReduction.rightReduction

}

object FDSet {

  private val someEmptySet = Some(Set.empty[String])

  def apply(fds: (Set[String], Set[String])*): FDSet =
    fds.foldLeft(empty)((r, fd) => r + fd)

  private val empty = new FDSet(Map.empty)

}
